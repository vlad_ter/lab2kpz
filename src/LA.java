import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LA {

    private String data;
    private String result;
    private final String REGEX = "([+-/\\\\*\\[\\]()])|([a-zA-Z0-9\\\\.]+)";
    private List<Element> local = new ArrayList<Element>();

    public LA(String data) {
        this.data = data;
        this.result = getDataOfLA();
    }

    public String getData() {
        return data;
    }

    public String getResult() {
        return result;
    }

    private String getDataOfLA() {
        StringBuilder sb = new StringBuilder();
        Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            local.add(new Element(matcher.group()));
        }
        int numberOfIdentifier = 1, numberOfConst = 1;
        for (Element el : local) {
            switch (el.getType().getCode()) {
                case (1): {
                    sb.append(String.format("(%d,%d)", el.getType().getCode(), numberOfIdentifier));
                    numberOfIdentifier++;
                    break;
                }
                case (2): {
                    sb.append(String.format("(%d,%d)", el.getType().getCode(), numberOfConst));
                    numberOfConst++;
                    break;
                }
                case (3): {
                    sb.append(String.format("(%d,%s)", el.getType().getCode(), el.getData()));
                    break;
                }
                case (4): {
                    sb.append(String.format("(%d,%s)", el.getType().getCode(), el.getData()));
                    break;
                }
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        LA a = new LA("alfa+x1*(2.836-x2[i])+4/2");
        System.out.println(a.getResult());
    }
}
