import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vladislav on 30.03.2015.
 */
public class Element {

    private TypeOfElement type;
    private String data;
    private static List<Pattern> logic = new ArrayList<Pattern>();

    static {
        logic.add(Pattern.compile("[a-zA-Z]+\\d?"));
        logic.add(Pattern.compile("(?<![a-zA-Z])[0-9\\.?]+"));
        logic.add(Pattern.compile("[^a-zA-Z0-9\\.\\[\\]\\(\\)]+|(/)"));
        logic.add(Pattern.compile("[\\[\\]\\(\\)]"));
    }

    public Element(String data) {
        this.data = data;
        analyze();
    }

    public TypeOfElement getType() {
        return this.type;
    }

    public String getData() {
        return this.data;
    }

    private void analyze() {
        for (Pattern item : logic) {
            Matcher matcher = item.matcher(data);
            if (matcher.find()) {
                int temp = logic.indexOf(item);
                switch (temp) {
                    case (0): {
                        type = TypeOfElement.IDENTIFIER;
                        return;
                    }
                    case (1): {
                        type = TypeOfElement.CONST;
                        return;
                    }
                    case (2): {
                        type = TypeOfElement.OPERATION;
                        return;
                    }
                    case (3): {
                        type = TypeOfElement.SYMBOL;
                        return;
                    }
                }
            }
        }
    }
}
