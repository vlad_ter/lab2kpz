/**
 * Created by Vladislav on 31.03.2015.
 */
public enum TypeOfElement {
    IDENTIFIER(1),
    CONST(2),
    OPERATION(3),
    SYMBOL(4);

    private int code = 0;

    TypeOfElement(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
